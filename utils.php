<?php
/** 
	Shared Utility routines 
	@author mgauthier@mtroyal.ca
*/


/********************************************************************************************
 * Purpose: appends a datestamped string with the given error string to a log file
 * Given: $error a string or an Exception object;
 *        $logfile, a filepath and name for the log file to write to
 *        $isException, an optional boolean flag: if true, the $error parameter contains and Exception object,
 *                      else it contains a string.
 * Effect: Appends the error message to the log file or throws and exception if it cannot write to file.
 **********************************************************************************************/ 
function logError($error, $logFile){		
		$logEntry = date("Y-m-d H:i:s") . "\n" . $error . "\n\n";
	try{
		// change log filename if apache is executing the script 
		$userInfo = posix_getpwuid(posix_getuid());
		$user = $userInfo['name'];    
		if(preg_match('/apache/i', $user)){	
			$logFile = preg_replace('|\.|', '_apache.', $logFile);
		}
		writeToFile($logEntry, $logFile, "a");
	} catch (Exception $e){
		error_log(print_r( $e->getMessage()));
		// do nothing
	}
}

/********************************************************************************************
 * Purpose: write the given string to the given file, using the given optional mode
 * Given: $string, a string;
 *        $file, a filepath and name
 *        $mode, an optional mode, assumes w for write if none is supplied. 
 *        See php documentation fopen possible mode values) 
 * Effect: Writes the given string to the given file. If optional mode is supplied, uses that mode, 
 *         else assume 'w' for 'write' mode
 **********************************************************************************************/ 
function writeToFile($string, $file, $mode = "w"){
	$handle = null;
	
	if(! file_exists($file)	){
		$handle = fopen($file, $mode);
		// set read/write permissions for the file
		chmod($file, 0664);
	} else {
		$handle = fopen($file, $mode);
	}
	
	if($handle){
		fwrite($handle, $string);
		fclose($handle);
		return true;
	} 
	return false;
}

?>