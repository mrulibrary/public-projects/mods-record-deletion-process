<?php
/********************************************************************************************
Purpose:Compares identifiers in current version of stored OAI-PMH records IDs file with those in the 
		previously stored file of identifiers and generates an xml file containing deletion records for any
		identifiers from the stored file that are not in the recent feed. Transfers the deletion xml file
		(which may contain no records, if no repository objects have been deleted) to the Alma transfer server
		where it will be picked up by a scheduled Discovery import profile.
		Side Effects: Makes backup copies with a .bak extension of stored files from previous week before overwriting
		them with new files. Sends an email report summarizing the outcome along with any errors encountered. Logs errors to an error file.
		
Created: 2020-06-15 by M.Gauthier
Script Logic:
Check for stored data csv file
	If file exists, 
		Create a backup copy
		Read the stored data file and load the contents into an associative array with the record identifiers as keys
Check for the stored xml feed file
	If file exists
		Make a backup copy with a .bak file extension
		Load the contents of the file into an array of record objects
		If the record object array is not empty
			Delete the old stored data file
			Loop through the records in the array 
				delete each record ID from the stored data array 
				Write the record ID, set name, title, and type to a new version of the stored data file
Check if there are any record identifier keys remaining in the storedData associative array
	If keys are found
		Make a backup copy of the existing deletion xml file (if it exists) with a .bak file extension
		Delete the old deletion xml file
		Create xml deletion records for all the remaining keys and write them to the xml deletion file
		Transfer the new deletion file to the alma transfer server

*** IMPORTANT NOTE: REPLACE variable values enclosed in double angle brackets <<>> with your own values ***
*********************************************************************************************/
define('LOCAL_PATH', '<</var/path_to_scripts>>'); // web server path to scripts
define('DATASTORE_PATH', LOCAL_PATH . '<<data/directory>>'); // path to directory where script output is stored
define('ERROR_LOG_FILE', DATASTORE_PATH . "mroar_error.log");
define('REMOTE_STORAGE_PATH', "<</home/your-path/your_directory/>>>>"); // remote server directory path
define('REPORT_EMAIL_SENDER', '<<MAIL SENDER NAME <your_sender.@yourdomain.com>>>');
define('REPORT_EMAIL_RECIPIENTS', '<<your_recipient.@yourdomain.com>>>');
define('ERR_LOG_PREFIX', 'Repository Deletion Process Error');

// INCLUDES
include (LOCAL_PATH . "utils.php"); // contains shared routines 
include (LOCAL_PATH . "sendEmail.php"); // contains shared routines 

// Array to hold the record objects
$recordArray = array();
$errorArray = array();
$storedArray = array();
$deletedArray = array();

// file declarations
$xmlFeedFile = DATASTORE_PATH . "mroarMods.xml"; //stored xml oai data
$storedDataFile = DATASTORE_PATH . "mroarRecords.csv"; // stored file with the parsed key data
$deletionFile = DATASTORE_PATH . "mroarDelete.xml"; // stored file with repository deletion records
$recordCount = 0;
$recordsWrittenToFile = 0;
$numDeletedRecords = 0;

// user for transferring deletion file to alma transfer server
$ssh_user = "<<some_user@some-server.com>>";

// check for the stored .csv file containing key data from the last feed  
if(file_exists($storedDataFile)){
	// copy the existing .csv file to a backup file
	$backupFile = $storedDataFile . ".bak";
	
	if(! copy($storedDataFile, $backupFile)){
		$errorMessage = ERR_LOG_PREFIX . " Could not make backup copy of file.";
		logError($errorMessage, ERROR_LOG_FILE);
		array_push($errorArray, $errorMessage);
	}
	
	if(is_readable($storedDataFile)){
		$lines = file($storedDataFile);
		// load contents of file into associative array
		$storedArray = storedFileToArray($lines, "\t");
		//var_dump($storedArray);
		
	} else {
		$errorMessage = ERR_LOG_PREFIX . " Could not read stored file: $storedDataFile.";
		logError($errorMessage, ERROR_LOG_FILE);
		array_push($errorArray, $errorMessage);		
	}
} else {
	$errorMessage = ERR_LOG_PREFIX . " No Mods .csv file found.";
	logError($errorMessage, ERROR_LOG_FILE);
	array_push($errorArray, $errorMessage);	
}

// read the stored OAI-PMH feed for the repository
$xmlSource = file_get_contents($xmlFeedFile);
if($xmlSource === FALSE){ // read contents fails
	$errorMessage = ERR_LOG_PREFIX . " Error fetching stored Mods xml feed file.";
	logError($errorMessage, ERROR_LOG_FILE);
	array_push($errorArray, $errorMessage);	
} else {
	// load brief records data(id, setSpec, title, and type) from XML file into an array of records
	$recordCount = parseXMLContent($xmlSource, $recordArray);

	// delete the stored .csv data file
	unlink($storedDataFile);
	
	// create a new data .csv file using the items in the OAI-PMH feed 
	$numRecs = (! empty($recordArray)) ? count($recordArray) : 0;
	//print_r("Num recs parsed: $numRecs$linebreak");
	
	// loop through the records found in the feed file and write them to a new .csv file
	for($i = 0; $i < $numRecs; $i++){
		
		// remove the id key from the stored array so that only ids for records that need to be deleted remain
		$key = (string)$recordArray[$i]->getRecordId();
		unset($storedArray[$key]);
		
		// create a tab delimited string from the record
		$fileString = $recordArray[$i]->outputTabDelimitedRecord();
		$fileString .= "\r\n"; // add carriage return and line feed
		
		// write the output string to file
		$result = file_put_contents($storedDataFile, $fileString, FILE_APPEND);
		if(! $result){
			$errorMessage = ERR_LOG_PREFIX . "Error writing to $storedDataFile: $fileString.";
			logError($errorMessage, ERROR_LOG_FILE);
			array_push($errorArray, $errorMessage);
		} else {
			$recordsWrittenToFile++;
		}
	}

	// create deleted records xml file (may contain no record nodes if no deletions since last run)
	$numDeletedRecords = generateDeletedItemFile($storedArray, $deletionFile, ERROR_LOG_FILE, $errorArray);

	$status; // var to hold status of exec command

	//transfer the deleted records xml file to the Alma transfer server 
	$transferResult = transferRemoteFile($ssh_user, REMOTE_STORAGE_PATH, $deletionFile, $status);
	if(! empty(trim($transferResult))){
		$errorMessage = ERR_LOG_PREFIX . " Error transferring file: $transferResult.";
		logError($errorMessage, ERROR_LOG_FILE);
		array_push($errorArray, $errorMessage);
	} else if($status != 0){
		$errorMessage = ERR_LOG_PREFIX . " Unable to execute SCP command to transfer file. Status: $status";
		logError($errorMessage, ERROR_LOG_FILE);
		array_push($errorArray, $errorMessage);
	}
}
// generate and send a job report
$reportName = "Mods Deletion Process Report for " . date('Y-m-d');
$reportOutput = generateReport($reportName, $recordCount, $recordsWrittenToFile, $numDeletedRecords, $errorArray);
sendMail(ERROR_LOG_FILE, REPORT_EMAIL_SENDER, REPORT_EMAIL_RECIPIENTS, $reportName, $reportOutput);
//print_r($reportOutput);


/**********************************************************************
   Purpose: Creates a job report as a string
   @param string $reportName
   @param int $numFeedRecords - number of records found in OAI-PMH feed file
   @param int $numWrittenRecords - number of records written to the brief record .csv file
   @param int $numDeletions - number of deletion records created in the xml deletion file
   @param array $errorArray - reference to an array of strings 
   @returns string containing the report 
***********************************************************************/
function generateReport($reportName, $numFeedRecords, $numWrittenRecords, $numDeletions, &$errorArray){
	$linebreak = "<br/>";
	$output = "$reportName$linebreak$linebreak";
	$output .= "$numFeedRecords records found in xml file.$linebreak";
	$output .= "$numWrittenRecords records stored in .csv file.$linebreak";
	$output .= "$numDeletions deletion records created in xml transfer file.$linebreak$linebreak";

	if(!empty($errorArray)){
		$output .= "ERRORS FOUND DURING PROCESSING: $linebreak$linebreak";
		foreach($errorArray as $error){
			$output .= "$error.$linebreak";
		}
	} else {
		$output .= "No errors found during processing.";
	}
	
	return $output;
}

/**********************************************************************
   Purpose: Parses the given xml content string and returns an array of record objects
   @param string $content
   @param array $recordArray - reference to an array
   @returns the number of records found in the given xml string
   Effects: loads the given array with the record objects found
***********************************************************************/
function parseXMLContent($content, &$recordArray){
	// load the given xml string
	$xmlObj = simplexml_load_string($content);

	$xmlNode = $xmlObj->ListRecords;
	$loopCount = 0;

	// loop through each record in the xml file
	foreach ($xmlNode->record as $rNode) {
		$typeArray = array();
		$title = "";
		$identifier = (string)$rNode->header->identifier;
		$set = (string)$rNode->header->setSpec;
		
		// parse out values from specific MODS tags inside the metadata tag
		foreach($rNode->metadata->mods as $mNode) {
			
			// get the primary title
			foreach($mNode->titleInfo as $titles){
				foreach($titles as $titleObj){
					// get primary title (title tag has no attribute set)
					if(! isset($titleObj->attributes()->type)){
						$title = (string)$titleObj;
					}
				}
			}
			
			// get the record types 
			foreach($mNode->genre as $genre){
				$type = (string)$genre;
				array_push($typeArray, $type);
			}
			
			// create a new record object and load it in to the record array
			$rec = new Record($set, $identifier, $title, $typeArray);
			array_push($recordArray, $rec);
		}
		$loopCount++;
	}
	return $loopCount;
}

/**********************************************************************
   Purpose: convert contents string  to associative array
   Returns an associative array with repository record ids as keys and an assoc array of set names, titles, and genres as values.
***********************************************************************/
function storedFileToArray($lines, $delimiter=',', $enclosure='"', $escape = '\\'){
  $data = array();
  foreach($lines as $line) {
	//  print_r($line);
	//  exit(0);
    $row = str_getcsv($line, $delimiter, $enclosure, $escape);
	
	$key = (string)$row[0];
	$type = isset($row[3]) ? (string)$row[3] : "";
	if(! array_key_exists($key, $data)){
		$data[$key] = array("SET" => (string)$row[1], "TITLE" => (string)$row[2], "TYPE" => $type);
	} 
  }

  return $data;
}



/***************************************************************************************************
 * Purpose: Generate a xml export file for records that need to be deleted
 * @param array $storedRecordArray - reference to an array of Record objects
 * @param string $outputFile - file path with filename for output file
 * @param string $errLog - file path with filename for error log file
 * @param array $errorArray - a reference for an array container for error messages
 * @returns integer representing the number of deletion records in the deleted record xml file
 * Effects: If $deleteIdArray is not empty:
    - if a deletion file with the given filename already exists, copies that file to a new file with the same name and an extension '.bak''
	- creates an xml deletion file with the given name containing records for identifiers in the given $deleteIdArray;
    - if a file read/write error occurs, loads the error message into the error array and logs it to the given $errLog file.
  **************************************************************************************************/
function generateDeletedItemFile(&$storedRecordArray, $outputFile, $errorLog, &$errorArray){
	$deletionRecordCount = 0;
	$deleteIdArray = array_keys($storedRecordArray);
	
	$output = "";
	$fileHead = '<ListRecords>';	
	$fileClose = '</ListRecords>';
	
	// loop through the array of item objects
	foreach($deleteIdArray as $item){
		// generate the xml string for the record and append it to the output string
		$output .= '<record><header status="deleted"><identifier>' . $item . '</identifier></header></record>';
		$deletionRecordCount++;
	}
	// wrap the output string in the enclosing tags for the xml file
	$content =  $fileHead . $output . $fileClose;
	
	try{
		if(file_exists($outputFile)){
			// make a backup copy of the old deletion file
			copy($outputFile, $outputFile . ".bak");
			unlink($outputFile);
		}
		// save the file to the server
		writeToFile($content, $outputFile, $mode = "w");
	} catch (Exception $e){	
		$errMsg = ERR_LOG_PREFIX . $e->getMessage();
		logError($errMsg, $errorLog);
		array_push($errorArray, $errMsg);
	}
	
	return $deletionRecordCount;
}

/*******************************************************************************
 * Purpose: Transfer given local file to remote server using SSH key
 * @param string $xferUser, the user:server string (e.g., user@remoteServer.domain.com)
 * @param string $remotePath - string containing path to the destination directory on remote server
 * @param string $localFile - file path and name for local file to transfer
 * @param string $errLog - locale error log file
 * @param array $errArray - reference to an array for reporting error messages
 * @returns string containing result of command to transfer file, may be empty

 ********************************************************************************/

function transferRemoteFile($xferUser, $remotePath, $localFile, &$commandStatus){
	$outputArray = array();
	$command = "scp $localFile $xferUser:$remotePath";
	return exec($command, $outputArray, $commandStatus);
}


class Record{
	private $record_id = "";
	private $record_set = "";
	private $record_title ="";
	private $record_type = array();
	
	function __construct($set, $id, $title, $type){
		$this->record_set = $set;
		$this->record_id = $id;
		$this->record_title = $title;
		$this->record_type = $type;
	}
	
	function getRecordId(){
		return $this->record_id;
	}
	
	function outputRecord(){
		$output = "";
		$types = (! is_null($this->record_type)) ? implode("; ",$this->record_type) : "NO TYPES";
		
		$elArray = array(
			'identifier' => $this->record_id,
			'set' => $this->record_set,
			'title' => $this->record_title,
			'type' => $types
		);
		

		foreach($elArray as $key => $val){
			$output .= "$key : $val ";
		}
		return $output;
	}
	
	function outputTabDelimitedRecord(){
		$output = "";
		$types = (! is_null($this->record_type)) ? implode("; ",$this->record_type) : "NO TYPES";
		
		$elArray = array(
			'identifier' => $this->record_id,
			'set' => $this->record_set,
			'title' => $this->record_title,
			'type' => $types
		);
		

		foreach($elArray as $key => $val){
			$output .= "$val\t";
		}
		$output = trim($output);
		return $output;
	}
}


?>

