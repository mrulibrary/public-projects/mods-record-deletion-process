# MODS Record Deletion Process

PHP scripts that read an OAI-PMH feed, compare the record IDs with previously stored records, and generate an XML file with deletion records for any deleted records.

Read the documentation in the ModsRepositoryRecordDeletionProcess.php file for more details.
